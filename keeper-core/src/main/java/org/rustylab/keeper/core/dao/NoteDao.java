package org.rustylab.keeper.core.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;

import org.rustylab.keeper.core.dto.NoteDetails;
import org.rustylab.keeper.core.dto.NoteSearchCriteria;
import org.rustylab.keeper.core.entity.Note;
import org.rustylab.keeper.core.service.UserResolver;

/**
 * Manages note objects at database level.
 */
@Repository("noteDao")
public class NoteDao {

    @Autowired
    private Sql2o sql2o;

    @Autowired
    private UserResolver userResolver;

    public List<NoteDetails> find(NoteSearchCriteria criteria) {
        final String selectQuery = buildQuery(criteria);
        
        try (Connection con = sql2o.open()) {
            Query q = con.createQuery(selectQuery);
            applyQueryParams(q, criteria);
            return q.executeAndFetch(NoteDetails.class);
        }
    }

    public Note find(long id) {
        String sql = "select * from Note where id = :id";
        try (Connection con = sql2o.open()) {
            return con.createQuery(sql)
                .addParameter("id", Long.valueOf(id))
                .executeAndFetchFirst(Note.class);
        }
    }

    private String buildQuery(NoteSearchCriteria criteria) {
        final String querySql = "select id, title, text from Note ";
        
        if (criteria == null) {
            return querySql + " where author = :author";
        }
        
        if (criteria.getAnyText() != null) {
            return querySql + " where n.author = :author and (n.text like '%:text%' or n.title like '%:title%')";
        }

        return querySql + " where n.id = :id and author = :author";
    }

    private void applyQueryParams(Query query, NoteSearchCriteria criteria) {
        query.addParameter("author", userResolver.currentUser().getId());

        if (criteria == null) {
            return;
        }
        
        if (criteria.getAnyText() != null) {
            query.addParameter("text", criteria.getAnyText());
            query.addParameter("title", criteria.getAnyText());
        }

        if (criteria.getId() != null) {
            query.addParameter("id", criteria.getId());
        }
    }

    public void create(Note note) {
        String sql = "insert into Note(title, text, author, created) "
                + "values (:title, :text, :author, :created)";
        try (Connection con = sql2o.open()) {
            con.createQuery(sql)
            .addParameter("title", note.getTitle())
            .addParameter("text", note.getText())
            .addParameter("author", note.getAuthor().getId())
            .addParameter("created", new Date())
            .executeUpdate();
        }
    }

    public void update(Note note) {
        String sql = "update Note set title = :title,"
                + " text = :text, updated = :updated "
                + " where id = :id";
        try (Connection con = sql2o.open()) {
            con.createQuery(sql)
            .addParameter("title", note.getTitle())
            .addParameter("text", note.getText())
            .addParameter("updated", new Date())
            .addParameter("id", note.getId())
            .executeUpdate();
        }
    }

}
