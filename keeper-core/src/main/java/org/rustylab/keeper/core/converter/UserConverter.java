package org.rustylab.keeper.core.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.sql2o.converters.Converter;
import org.sql2o.converters.ConverterException;

import org.rustylab.keeper.core.dao.UserDao;
import org.rustylab.keeper.core.entity.User;

@Component
public class UserConverter implements Converter<User> {

    @Autowired
    @Lazy
    private UserDao userDao;

    @Override
    public User convert(Object userId) throws ConverterException {
        if (userId instanceof Number) {
            Long authorId = ((Number) userId).longValue();
            return userDao.findById(authorId);
        }
        return null;
    }

    @Override
    public Object toDatabaseParam(User user) {
        if (user == null) {
            return null;
        }
        return user.getId();
    }

}
