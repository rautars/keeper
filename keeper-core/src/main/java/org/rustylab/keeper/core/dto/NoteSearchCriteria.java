package org.rustylab.keeper.core.dto;

import java.io.Serializable;

/**
 * Note search criteria fields.
 */
public class NoteSearchCriteria implements Serializable {

    private static final long serialVersionUID = 2776120710201468182L;

    private Long id;

    private String anyText;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAnyText() {
        return anyText;
    }

    public void setAnyText(String anyText) {
        this.anyText = anyText;
    }

}
