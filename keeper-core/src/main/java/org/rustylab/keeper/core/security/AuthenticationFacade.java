package org.rustylab.keeper.core.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import org.rustylab.keeper.core.security.AuthenticationFacade;

/**
 * Authentication facade helps code testability while security context static
 * state is hidden.
 */
@Component("authenticationFacade")
public class AuthenticationFacade {

    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}
