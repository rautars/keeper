package org.rustylab.keeper.core.validator;

import org.springframework.stereotype.Component;

import org.rustylab.keeper.core.entity.Note;
import org.rustylab.keeper.core.validator.NoteValidator;

/**
 * Validates Note entity.
 */
@Component("noteValidator")
public class NoteValidator {

    /**
     * Validates note entity for mandatory fields.
     * 
     * @param note
     *            note entity which needs to be validated.
     * @throws IllegalArgumentException
     *             upon failed validation.
     */
    public void validate(Note note) {
        if (!isTitleOrTextFilled(note)) {
            throw new IllegalArgumentException();
        }
    }

    private boolean isTitleOrTextFilled(Note note) {
        if (note == null) {
            return false;
        }

        if (isNullOrEmpty(note.getTitle()) && isNullOrEmpty(note.getText())) {
            return false;
        }

        return true;
    }
    
    private boolean isNullOrEmpty(String arg) {
        return arg == null || "".equals(arg);
    }
}
