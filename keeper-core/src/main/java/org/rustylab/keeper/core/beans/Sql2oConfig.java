package org.rustylab.keeper.core.beans;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.sql2o.Sql2o;
import org.sql2o.converters.Converter;
import org.sql2o.quirks.NoQuirks;

import org.rustylab.keeper.core.converter.UserConverter;
import org.rustylab.keeper.core.entity.User;

@Configuration
public class Sql2oConfig {

    @Autowired
    UserConverter userConverter;
    
    @Autowired
    @Qualifier("dataSource")
    DataSource dataSource;

    @SuppressWarnings("rawtypes")
    @Bean
    public Sql2o getSql2o() {
        final Map<Class, Converter> converters = new HashMap<>();
        converters.put(User.class, userConverter);
        return new Sql2o(dataSource, new NoQuirks(converters));
    }

}
