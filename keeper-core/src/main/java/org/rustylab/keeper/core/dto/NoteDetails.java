package org.rustylab.keeper.core.dto;

import java.io.Serializable;

public class NoteDetails implements Serializable {
    private static final long serialVersionUID = -4125449540002019814L;

    private Long id;

    private String title;

    private String text;

    public NoteDetails() {
    }

    public NoteDetails(Long id, String title, String text) {
        super();
        this.id = id;
        this.title = title;
        this.text = text;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
