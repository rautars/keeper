package org.rustylab.keeper.ui.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import org.rustylab.keeper.core.dto.NoteDetails;
import org.rustylab.keeper.core.service.NoteService;

@Controller
@ManagedBean
@ViewScoped
public class NoteForm {

    private NoteDetails note;

    @Autowired
    @Qualifier("noteService")
    private NoteService noteService;

    public void init(NoteDetails editNote) {
        if (editNote != null) {
            note = editNote;
        } else {
            note = new NoteDetails();
        }
    }

    public String saveNote() {
        noteService.saveNote(note);
        return "index?faces-redirect=true";
    }

    public NoteDetails getNote() {
        return note;
    }

    public void setNote(NoteDetails note) {
        this.note = note;
    }

}
