package org.rustylab.keeper.ui.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.stereotype.Controller;

import org.rustylab.keeper.core.dto.NoteDetails;


@Controller
@ManagedBean
@SessionScoped
public class FlatBean {

    private NoteDetails editNote;

    public void resetEditNotes() {
        editNote = null;
    }

    public NoteDetails getEditNote() {
        return editNote;
    }

    public void setEditNote(NoteDetails editNote) {
        this.editNote = editNote;
    }

}
