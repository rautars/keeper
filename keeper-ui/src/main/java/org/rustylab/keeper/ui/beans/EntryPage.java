package org.rustylab.keeper.ui.beans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import org.rustylab.keeper.core.dto.NoteDetails;
import org.rustylab.keeper.core.service.NoteService;

@Controller
@ManagedBean
@ViewScoped
public class EntryPage implements Serializable {
    private static final long serialVersionUID = 6566649404575146563L;

    private List<NoteDetails> notes;

    private String searchTerm;

    @Autowired
    @Qualifier("noteService")
    private NoteService noteService;

    @PostConstruct
    public void init() {
        loadNotes();
    }

    private void loadNotes() {
        notes = noteService.returnAllNotes();
    }

    public List<NoteDetails> getNotes() {
        return notes;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

}
